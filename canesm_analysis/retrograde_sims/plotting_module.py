import os
import numpy as np
import xarray as xr
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.pyplot as plt
import matplotlib as mpl
import pandas as pd
from scipy import stats
import calendar




def leap_year(year, calendar='standard'):
    """
    Determine if year is a leap year
    adapted from http://xarray.pydata.org/en/stable/examples/monthly-means.html
    """
    leap = False
    if ((calendar in ['standard', 'gregorian','proleptic_gregorian', 'julian']) and (year % 4 == 0)):
        leap = True
        if ((calendar == 'proleptic_gregorian') and (year % 100 == 0) and (year % 400 != 0)):
            leap = False
        elif ((calendar in ['standard', 'gregorian']) and (year % 100 == 0) and (year % 400 != 0) and (year < 1583)):
            leap = False

    return leap


def get_dpm(time, calendar='noleap'):
    """
    return a array of days per month corresponding to the months provided in `month_lengths`
    adapted from http://xarray.pydata.org/en/stable/examples/monthly-means.html
    """
    dpm = {'noleap': [0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],
           'standard': [0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]}
    month_length = np.zeros(len(time), dtype=np.int)
    cal_days = dpm[calendar]
    for i, (month, year) in enumerate(zip(time.month, time.year)):
        month_length[i] = cal_days[month]
        if leap_year(year,calendar=calendar) and month==2:
            month_length[i] += 1

    return month_length


def get_month_weighted_annual_means(ds, calendar, start_date, end_date):
    """
    calculate the weighted global annual means from monthly data 
    """
    
    ds = ds.sortby('time')  
    try:
        selyears = ds.sel(time=slice(start_date, end_date))
    except TypeError as error:
        ds['time'] = pd.to_datetime(ds['time'].astype(int),format='%Y') 
        selyears = ds.sel(time=slice(start_date, end_date))
    
    month_length = xr.DataArray(get_dpm(selyears.time.to_index(),calendar=calendar), coords=[selyears.time], name='month_length')
    weights = month_length.groupby('time.year')/ month_length.groupby('time.year').sum()
    weighted_annmeans = (selyears * weights).groupby('time.year').sum(dim='time',min_count=1)
    
    return weighted_annmeans


def get_weighted_globalmean(ds, calendar, start_date, end_date):
    """
    calculate the weighted global overall time mean from monthly data 
    """
    weighted_annmeans = get_month_weighted_annual_means(ds, calendar, start_date, end_date)
    weighted_globalmean = weighted_annmeans.mean(dim='year')
    
    return weighted_globalmean



def get_seasonal_zm(ds, calendar, season):
    """
    calculate a weighted seasonal zonal mean from monthly data 
    """
    month_length = xr.DataArray(get_dpm(ds.time.to_index(), calendar=calendar), coords=[ds.time], name='month_length')
    grouped_month_lengths = month_length.groupby('time.season') 
    weights = grouped_month_lengths/grouped_month_lengths.sum()
    weighted_seasonalmeans = (ds * weights).groupby('time.season').sum(dim='time',min_count=1)
    if 'lon' in ds.dims:
        weighted_seasonal_zm = weighted_seasonalmeans.sel(season=season).mean(dim=['lon'])
    elif 'x' in ds.dims:
        weighted_seasonal_zm = weighted_seasonalmeans.sel(season=season).mean(dim=['x'])
    else:
        raise ValueError('Neither lon nor x in dimensions of data to zonal mean')
    return weighted_seasonal_zm

def get_seasonal(ds, calendar, season):
    """
    calculate a weighted seasonal from monthly data 2d data (map)
    """
    month_length = xr.DataArray(get_dpm(ds.time.to_index(), calendar=calendar), coords=[ds.time], name='month_length')
    grouped_month_lengths = month_length.groupby('time.season') 
    weights = grouped_month_lengths/grouped_month_lengths.sum()
    weighted_seasonalmeans = (ds * weights).groupby('time.season').sum(dim='time',min_count=1)
    weighted_seasonal = weighted_seasonalmeans.sel(season=season)

    return weighted_seasonal


def addCyclicPoint(xarray_obj, dim, period=None):
    """
    Add a cyclic longitude point for data that doesn't wrap back to zero. Avoids seam at 0 or 180 longitude
    """
    if period is None:
        period = xarray_obj.sizes[dim] * xarray_obj.coords[dim][:2].diff(dim).item()
    first_point = xarray_obj.isel({dim: slice(1)})
    first_point.coords[dim] = first_point.coords[dim]+period
                                    
    return xr.concat([xarray_obj, first_point], dim=dim)


def Linregress3D(x, y):
    """
    Input: Two xr.Datarrays of any dimensions with the first dim being time. 
    Thus the input data could be a 1D time series, or for example, have three 
    dimensions (time,lat,lon). 
    Datasets can be provided in any order, but note that the regression slope 
    and intercept will be calculated for y with respect to x.
    Output: Covariance, correlation, regression slope and intercept, p-value, 
    and standard error on regression between the two datasets along their 
    aligned time dimension.  
    ref: https://hrishichandanpurkar.blogspot.com/2017/09/vectorized-functions-for-correlation.html
    """ 
    #Ensure that the data are properly alinged to each other. 
    x,y = xr.align(x,y)
    
    #Compute data length, mean and standard deviation along time axis: 
    n = y.notnull().sum(dim='year')
    xmean = x.mean(axis=0)
    ymean = y.mean(axis=0)
    xstd  = x.std(axis=0)
    ystd  = y.std(axis=0)

    #Compute covariance along time axis
    cov   =  np.sum((x - xmean)*(y - ymean), axis=0)/(n)

    #Compute correlation along time axis
    cor   = cov/(xstd*ystd)

    #Compute regression slope and intercept:
    slope     = cov/(xstd**2)
    intercept = ymean - xmean*slope  

    #Compute P-value and standard error
    #Compute t-statistics
    tstats = cor*np.sqrt(n-2)/np.sqrt(1-cor**2)
    stderr = slope/tstats

    from scipy.stats import t
    pval   = t.sf(tstats, n-2)*2
    pval   = xr.DataArray(pval, dims=cor.dims, coords=cor.coords)

    return cov,cor,slope,intercept,pval,stderr


def Linregress3Dds(gp):
    """
    Formats a groupby object so it can be ingested by Linregress3D
    """
    gp = gp.rename({'time':'year'})
    x = gp['year.year']
    y = gp
    cov,cor,slope,intercept,pval,stderr = Linregress3D(x, y)
    
    return slope


def add_cbar(position, fig, ax, co, size='5%', pad=0.075, **kwargs):
    """
    Adds a colorbar to an axes
    """
    divider = make_axes_locatable(ax)
    ax_cb = divider.append_axes(position, size=size, pad=pad, axes_class=plt.Axes)
    fig.add_axes(ax_cb)

    if position in ['top', 'bottom']:
        orientation='horizontal'
    if position in  ['right','left']:
        orientation='vertical'
    
    plt.colorbar(co, cax=ax_cb, orientation=orientation, **kwargs)

    if position == 'left':
        ax_cb.yaxis.set_ticks_position('left')
        ax_cb.yaxis.set_label_position('left')
        
        
        
def inner_gs(outer_gs, labely=False, labelx=False):
    """
    add an inner gridspec instance
    """
    inner_grid = outer_gs.subgridspec(1, 2, wspace=0.1, hspace=0.0, width_ratios=[0.95,0.05])
    axt = plt.subplot(inner_grid[0, 0])
    axc = plt.subplot(inner_grid[:, 1])
    
    axt.set_yscale('log')
    axt.set_ylim([1000,10])       
    axt.xaxis.set_tick_params(labelbottom=labelx, bottom=True)
    axt.yaxis.set_tick_params(labelleft=labely, left=True)

    if labelx:
        axt.set_xticks(np.arange(-90,91,30)) 
        axt.set_xticklabels(('90S','60S','30S','EQ','30N','60N','90N'))
        axt.set_xlabel('Latitude')
        
    if labely:
        axt.set_yticks([1000,500,200,100, 50, 20, 10])
        axt.get_yaxis().set_major_formatter(mpl.ticker.ScalarFormatter())
        axt.set_ylabel('Pressure (hPa)', labelpad=2)
        
    return axt, axc    


def inner_gs_vardepth(outer_gs, labely=False, labelx=False):
    """
    add a pair of inner gridspecs to allow depth mapping at two diff intervals
    """
    inner_grid = outer_gs.subgridspec(2, 2, wspace=0.1, hspace=0.0, width_ratios=[0.95,0.05], height_ratios=[0.4,0.6])
    axm = plt.subplot(inner_grid[1, 0])
    axt = plt.subplot(inner_grid[0, 0], sharex=axm)
    axc = plt.subplot(inner_grid[:, 1])
    
    axt.set_ylim([1000,0])
    axt.spines['bottom'].set_visible(False)
    axt.set_xticks([])
    axt.xaxis.set_tick_params(labelbottom=False,  bottom=False)
    axt.yaxis.set_tick_params(labelleft=labely, left=True)  

    axm.set_ylim([5500,1000])
    axm.spines['top'].set_visible(False)
    axm.xaxis.set_tick_params(labelbottom=labelx, bottom=True) 
    axm.yaxis.set_tick_params(labelleft=labely, left=True) 
    
    if labelx:
        axm.set_xticks(np.arange(-90,91,30)) 
        axm.set_xticklabels(('90S','60S','30S','EQ','30N','60N','90N'))
        axm.set_xlabel('Latitude')
    if labely:
        axt.set_yticks([0, 250, 500, 750]) 
        axm.set_yticks([1000, 2000, 3000, 4000, 5000]) 
        axm.set_ylabel('Depth (m)')
        axm.yaxis.set_label_coords(-0.15,0.9)
    
    return axt, axm, axc       


def renameCoords(dataset):
    """
    renames lat and lon coords if the ds has them as 'latitude' and 'longitude'
    """
    if 'longitude' in dataset.coords:
        print('renaming longitude')
        dataset = dataset.rename({'longitude':'lon'})
    if 'latitude' in dataset.coords:       
        print('renaming latitude')
        dataset = dataset.rename({'latitude':'lat'})
        
    return dataset


def get_ts_data(month, ds_list, start, end, calendar='noleap'):
    """
    takes in a list of datasets and returns timeseries of annual or 
    monthly means (for a single month only).
    
    Parameters:
    -----------
    month : int or 'all'
        month as int, or 'all' for annual means
    ds_list : list of datasets  (typically one per realization)
    
    Returns:
    -----------
    rels : ds
       the concatenated realizations 
    relmean : ds   
       the realization mean
        
    """
    if month == 'all':
        concat_rels = xr.concat(ds_list,dim='realization')
        ann_rels = get_month_weighted_annual_means(concat_rels, calendar, start, end)
        rels = ann_rels.rename({'year':'time'})
    else:    
        concat_rels = xr.concat(ds_list,dim='realization')
        selmonth_rels = concat_rels.sel(time=concat_rels['time.month'] == month) 
        ann_rels = selmonth_rels.groupby('time.year').mean('time')
        rels = ann_rels.rename({'year':'time'})
    
    relmean = rels.mean('realization')   

    return rels, relmean

def get_normfit(month,ds_list):
    """
    takes in a list of datasets with either annual or monthly trends, and a month.
    If the month == 'all', returns the trends as well as coefficients for a normal distr fit to the trends.
    If the month is an int, first extracts trends for that month only, then returns the extracted values and 
    the coefficients for a normal distr fit to the those values.
    
    Parameters:
    -----------
    month : int or 'all'
        month as int, or 'all' for annual trends
    ds_list : list of datasets (typically one per realization)
    
    Returns:
    -----------
    trends : list
        values used for the fit
    
    mu : float
        mean of the normal distr fit to trends
        
    sig : float
        std dev of the normal distr fit to trends
    """
    
    if month == 'all':
        trends = np.array(ds_list)
        mu, sig = stats.norm.fit(trends[np.isfinite(trends)])        
    else:    
        concat_rels = xr.concat(ds_list,dim='realization')
        trends = concat_rels.sel(month=month) 
        mu, sig = stats.norm.fit(trends[np.isfinite(trends)])

    return trends, mu, sig    

def ylabel_month(month):
    """
    formats month int as appropriate string, or as 'Annual' for 'all' months
    """
    month_name = None
    if isinstance(month, int):
        month_name = calendar.month_name[month]
    if month == 'all':
        month_name = 'Annual'
        
    return month_name


def replace_dim(da, olddim, newdim):
    """Replace a dimension with a new dimension

    PARAMETERS
    ----------
    da : xarray.DataArray
    olddim : str
        name of the dimension to replace
    newdim : xarray.DataArray
        dimension to replace it with

    RETURNS
    -------
    da_new : xarray.DataArray
    """
    
    renamed = da.rename({olddim: newdim.name})
    # note that alignment along a dimension is skipped when you are overriding
    # the relevant coordinate values
    renamed.coords[newdim.name] = newdim

    return renamed