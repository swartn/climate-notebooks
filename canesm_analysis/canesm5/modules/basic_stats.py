import os 
import xarray as xr
import numpy as np
from scipy import stats
import json
import sqlite3


## main - calculate stats and write to file + db 

def calc_stats_and_write(obs_datasets, obs_fulldatasets, obs_names, model_datasets, model_fulldatasets, model_names, stats_names, varslist, weight, xyzt, av_dims, start, end, calc_skill_score=True, use_model_std=False, annual_data=False):

    #calc stats for zipped lists and add to json file and database
    #db connection
    conn = sqlite3.connect('stats.db')
    c = conn.cursor()       
    
    for obs_data, obs_fulldata, obs_name, model_data, model_fulldata, model_name, stats_name, var in zip(obs_datasets, obs_fulldatasets, obs_names, model_datasets, model_fulldatasets, model_names, stats_names, varslist):
        
        print(var, model_name, obs_name)
        
        #get basic stats
        stats = get_basic_stats(model_data, obs_data, weights=weight)
        
        #get skill scores
        if calc_skill_score:
            try:
                skill_scores = get_skill_scores(model_data, obs_data, model_fulldata=model_fulldata, obs_fulldata=obs_fulldata, 
                                                av_dims=av_dims, use_model_std=use_model_std, annual_data=annual_data, stats=stats)
                stats.update(skill_scores)
            except Exception as e:
                print('skill scores not calculated')
                print(e)
                stats['S_bias'] = None
                stats['S_rmse'] = None
                stats['S_dist'] = None
        else:
            stats['S_bias'] = None
            stats['S_rmse'] = None
            stats['S_dist'] = None
        
        #calc S_dist separately since it doesn't need the full datasets
        S_dist = _calc_dist_skillscore(stats)
        stats['S_dist'] = S_dist
     
        #write to json file
        print(stats)
        write_stats_to_file(stats_name, stats, var=var, model=model_name, obs=obs_name, startdate=start, enddate=end, xyzt=xyzt)
        
        #write to db
        values = {'var':var, 
                  'model':model_name, 
                  'obs':obs_name, 
                  'startdate':start, 
                  'enddate':end, 
                  'bias':stats['bias'], 
                  'RMSE':stats['RMSE'], 
                  'CRMSE': stats['CRMSE'],
                  'corrcoeff':stats['corrcoeff'], 
                  'model_std':stats['model_std'], 
                  'obs_std':stats['obs_std'], 
                  'S_bias':stats['S_bias'], 
                  'S_rmse':stats['S_rmse'],
                  'S_dist':stats['S_dist'],
                  'xyzt': xyzt,
                  }
        c.execute(('''INSERT INTO basic_stats (var, model, obs, startdate, enddate, bias, RMSE, CRMSE, corrcoeff, model_std, obs_std, S_bias, S_rmse, S_dist, xyzt) 
                      VALUES (:var, :model, :obs, :startdate, :enddate, :bias, :RMSE, :CRMSE, :corrcoeff, :model_std, :obs_std, :S_bias, :S_rmse, :S_dist, :xyzt)'''), values)
        conn.commit()
    conn.close()

    
#write to json file

def write_stats_to_file(name, statsdict, var=None, obs=None, model=None, startdate=None, enddate=None, xyzt=None):
    """
    write stats to json file
    """
    statsdict['var'] = var
    statsdict['model'] = model
    statsdict['obs'] = obs
    statsdict['startdate'] = startdate
    statsdict['enddate'] = enddate
    statsdict['xyzt'] = xyzt

    new_statsdict = {name: statsdict}
    
    if os.path.isfile('stats.json'):
        with open('stats.json', 'r',) as f:
            statsdict_f = json.load(f)
        statsdict_f.update(new_statsdict)
        with open('stats.json', 'w') as f:
            json.dump(statsdict_f, f)      
    else:
        with open('stats.json', 'w') as f:
            json.dump(new_statsdict, f)     


            
#basic stats 

def get_basic_stats(model_data, obs_data, weights=None):  
    """
    get basic statistics on model and obs means
    """
    #flatten model_mean and obs_mean arrays
    model_data_flat   = np.ndarray.flatten(model_data.values)
    obs_data_flat     = np.ndarray.flatten(obs_data.values)
       
    #find indices where model_mean or obs_mean are nan
    model_indices = ~np.isnan(model_data_flat)
    obs_indices = ~np.isnan(obs_data_flat)
    combined_indices = np.logical_and(model_indices,obs_indices)
    
    #get weights
    if weights is not None:
        if isinstance(weights, xr.core.dataarray.DataArray):
            weights = weights.values
        weights_flat = np.ndarray.flatten(weights)[combined_indices]
    else:
        weights_flat = None
    
   
    #calc basic stats for all points where neither model or obs are nan
    wtdbias       = _calc_wtdbias(model_data_flat[combined_indices], obs_data_flat[combined_indices], weights=weights_flat)
    wtdRMSE       = _calc_wtdRMSE(model_data_flat[combined_indices], obs_data_flat[combined_indices], weights=weights_flat)
    wtdCRMSE      = _calc_wtdcenteredRMSE(model_data_flat[combined_indices], obs_data_flat[combined_indices], weights=weights_flat)
    corrcoeff     = _calc_wtdpearsonr(model_data_flat[combined_indices], obs_data_flat[combined_indices], weights=weights_flat)
    model_wtdstd  = _calc_wtdstd(model_data_flat[combined_indices], weights=weights_flat)
    obs_wtdstd    = _calc_wtdstd(obs_data_flat[combined_indices], weights=weights_flat)
    
    #build statsdict
    statsdict = {'bias':float(wtdbias),
                 'RMSE':float(wtdRMSE),
                 'CRMSE': float(wtdCRMSE),
                 'corrcoeff':float(corrcoeff),
                 'model_std':float(model_wtdstd),
                 'obs_std':float(obs_wtdstd),
                }
    
    return statsdict
              

            
def _calc_wtdbias(model_data,obs_data,weights=None):  
    """
    Calculate the weighted bias (model-obs)
    """
    wtdbias = np.average((model_data - obs_data), weights=weights)

    return wtdbias
            
        

def _calc_wtdRMSE(model_data,obs_data,weights=None):  
    """
    Calculate the weighted RMSE
    """
    avg_sqdresdls = np.average((model_data - obs_data)**2, weights=weights)
    wtdRMSE = np.sqrt(avg_sqdresdls)

    return wtdRMSE



def _calc_wtdcenteredRMSE(model_data,obs_data,weights=None):
    """
    Calculate the weighted centered RMSE
    """

    model_wtdavg  = np.average(model_data, weights=weights)
    obs_wtdavg    = np.average(obs_data, weights=weights)
    avg_sqdresdls = np.average(((model_data - model_wtdavg) - (obs_data - obs_wtdavg))**2, weights=weights)
    wtdcRMSE      = np.sqrt(avg_sqdresdls)
 
    return wtdcRMSE



def _calc_wtdpearsonr(model_data,obs_data,weights=None):
    """
    Calculate the weighted Pearson correlation coefficient
    """
    model_wtdavg      = np.average(model_data, weights=weights)
    model_wtdvariance = np.average((model_data - model_wtdavg)**2, weights=weights)
    model_wtdstd      = np.sqrt(model_wtdvariance)

    obs_wtdavg      = np.average(obs_data, weights=weights)
    obs_wtdvariance = np.average((obs_data - obs_wtdavg)**2, weights=weights)
    obs_wtdstd      = np.sqrt(obs_wtdvariance)

    corrcoeff =  np.average((obs_data-obs_wtdavg)*(model_data-model_wtdavg), weights=weights) / (obs_wtdstd*model_wtdstd)
    #sanity check - corrcoeff2, p_value  = stats.pearsonr(model_data, obs_data)

    return corrcoeff



def _calc_wtdstd(data,weights=None):
    """
    Calculate the weighted std deviation
    """
    wtdavg      = np.average(data, weights=weights)
    wtdvariance = np.average((data-wtdavg)**2, weights=weights)
    wtdstd      = np.sqrt(wtdvariance)

    return wtdstd



            
##ilamb skill scores

def get_skill_scores(model_data=None, obs_data=None, model_fulldata=None, obs_fulldata=None, weights=None, av_dims=['time'], use_model_std=False, annual_data=False, stats=None):
    
    #resample full datasets to first of each month or year for comparisons
    
    if annual_data:
        model_fulldata = _resample_yearly_data(model_fulldata)
        if use_model_std:
            obs_fulldata = None
        else:
            obs_fulldata   = _resample_yearly_data(obs_fulldata)
        
    else:
        model_fulldata = _resample_monthly_data(model_fulldata)
        if use_model_std:
            obs_fulldata = None
        else:
            obs_fulldata = _resample_monthly_data(obs_fulldata)
        
    #flatten model_data and obs_data arrays
    model_data_flat   = np.ndarray.flatten(model_data.values)
    obs_data_flat     = np.ndarray.flatten(obs_data.values)

    #find indices where model_data or obs_data are nan
    model_indices = ~np.isnan(model_data_flat)
    obs_indices = ~np.isnan(obs_data_flat)
    combined_indices = np.logical_and(model_indices,obs_indices)
    
    #get skill scores
    S_bias = _calc_bias_skillscore(model_data_flat, obs_data_flat, combined_indices, model_fulldata, obs_fulldata, av_dims)
    S_rmse = _calc_rmse_skillscore(model_data, obs_data, model_fulldata, obs_fulldata, av_dims)
    
    statsdict = {'S_bias':float(S_bias),
                 'S_rmse':float(S_rmse),
                }
    
    return statsdict
            


def _calc_bias_skillscore(model_data_flat, obs_data_flat, combined_indices, model_fulldata, obs_fulldata, av_dims):   
    """
    Calculate a skill score for bias following ilamb conventions
    """
    #calc bias
    bias = model_data_flat[combined_indices] - obs_data_flat[combined_indices]
    
    #calc obs stddev, but use model data if obs data not available
    if obs_fulldata is None:
        stddev   = model_fulldata.std(dim = av_dims)
        std_flat = np.ndarray.flatten(stddev.values)
    else:
        stddev   = obs_fulldata.std(dim = av_dims)
        std_flat = np.ndarray.flatten(stddev.values)
    
    #calc skill score
    e_bias = np.abs(bias)/std_flat[combined_indices]
    s_bias = np.exp(-e_bias)
    S_bias = np.nanmean(s_bias)

    return S_bias


def _calc_rmse_skillscore(model_data, obs_data, model_fulldata, obs_fulldata, av_dims):   
    """
    Calculate a skill score for bias following ilamb conventions
    """
    #if timeseries of obs data is available calculate a CRMSE (data in x,y,z,t - data in x,y,z), if not, calculate just the RMSE (data in x,y,z)
    if obs_fulldata is None:
        mean_sqd_diffs = ((model_data - obs_data)**2)
    else:
        mean_sqd_diffs = (((model_fulldata - model_data) - (obs_fulldata - obs_data))**2).mean(dim=av_dims)
    
    crmse_flat = np.ndarray.flatten(np.sqrt(mean_sqd_diffs.values))
    indices = ~np.isnan(crmse_flat)
    crmse = crmse_flat[indices]
    
    #calc obs stddev, but use model data if obs data not available
    if obs_fulldata is None:
        stddev   = model_fulldata.std(dim = av_dims)
        std_flat = np.ndarray.flatten(stddev.values)
    else:
        stddev   = obs_fulldata.std(dim = av_dims)
        std_flat = np.ndarray.flatten(stddev.values)
    
    #calc skill score
    e_rmse = np.abs(crmse)/std_flat[indices]
    s_rmse = np.exp(-e_rmse)
    S_rmse = np.nanmean(s_rmse)

    return S_rmse


def _calc_dist_skillscore(stats):   
    """
    Calculate a skill score for spatial distribution following ilamb conventions
    """
    model_std = stats['model_std'] #np.nanstd(model_fulldata)#
    obs_std = stats['obs_std'] #np.nanstd(obs_fulldata)#
    corrcoeff = stats['corrcoeff']
    
    std_ratio = model_std/obs_std
    S_dist = 2*(1+corrcoeff)/(std_ratio+1/(std_ratio))**2
        
    return S_dist



## additional methods

def _resample_monthly_data(dataset):
    """
    resample a list of datasets to monthly intervals (1st of the month)
    """

    dataset = dataset.resample(time='1MS').mean(dim='time') #resample so all timstamps the same (eg all on 1st of month)
    try:
        datetimeindex = dataset.indexes['time'].to_datetimeindex() #set all datetime types to datetime64 for S_rmse calc
        dataset['time'] = datetimeindex
    except Exception as e:
        print(e)
    
    return dataset


def _resample_yearly_data(dataset):
    """
    resample a list of datasets to monthly intervals (1st of the month)
    """
    dataset = dataset.resample(time='1AS').mean(dim='time') #resample so all timstamps the same (eg all on 1st of month)
    try:
        datetimeindex = dataset.indexes['time'].to_datetimeindex() #set all datetime types to datetime64 for S_rmse calc
        dataset['time'] = datetimeindex
    except Exception as e:
        print(e)
    
    return dataset
